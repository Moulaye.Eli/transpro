package com.example.demo.dao;



import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.entities.Camion;

public interface CamionRepository extends CrudRepository<Camion, Long>{
	
/*	@Query("select u from Camion u where u.camionneur.id=?1")
	Iterable<Camion> getAllCamionByIdCamionneur(long id);*/
	
	Optional<Camion>  findByMatricule(String matricule);
	
}
