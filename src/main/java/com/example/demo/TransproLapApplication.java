package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.example.demo.dao.CamionRepository;
import com.example.demo.entities.Camion;

@SpringBootApplication
public class TransproLapApplication extends SpringBootServletInitializer implements CommandLineRunner { 

	public static void main(String[] args) {
		SpringApplication.run(TransproLapApplication.class, args);
	}

	@Autowired
	CamionRepository camionRepository ;
	
	@Override
	public void run(String... args) throws Exception {

		
		camionRepository.save(new Camion("0012AA01", "T900", "Mercedes", 200) );
		camionRepository.save(new Camion("0013AA01", "T950", "Mercedes", 200) );
		camionRepository.save(new Camion("0014AA01", "T900", "Mercedes", 150) );
		
	}
	
	  @Override
	  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	    return application.sources(TransproLapApplication.class);
	  }
}
