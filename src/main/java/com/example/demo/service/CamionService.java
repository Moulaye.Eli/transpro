package com.example.demo.service;

import com.example.demo.entities.Camion;

public interface CamionService {
	
	public Camion ajouterCamion(Camion c)throws Exception;
	
	public Iterable<Camion>  listerCamion();
	
	public Camion majCamion(Camion c) throws Exception ;
	
	public void suppCamion(long id);
	
	
}
