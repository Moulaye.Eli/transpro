package com.example.demo.service.imp;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.CamionRepository;
import com.example.demo.entities.Camion;
import com.example.demo.service.CamionService;

@Service
//("CamionServiceImp")
public class CamionServiceImp implements CamionService {
	
	@Autowired
	CamionRepository camionRepository ;
	

	
	
	@Override
	public Camion ajouterCamion(Camion c) throws Exception {
		
		Optional<Camion> optional=camionRepository.findByMatricule(c.getMatricule());
		if(optional.isPresent())
		{
				throw new Exception("matricule");
			
		}else {
			return 	camionRepository.save(c);
		}
		
		
	}



	@Override
	public Iterable<Camion> listerCamion() {
		return camionRepository.findAll();
	}


	@Override
	public Camion majCamion(Camion c) throws Exception {
		
	 Optional<Camion> optional=	camionRepository.findById(c.getId());
	 if(optional.isPresent()) {
		 Camion camion=optional.get();
		 String matricule=c.getMatricule();
		 if(!camion.getMatricule().equals(matricule)) {
			 Optional<Camion> matExiste=camionRepository.findByMatricule(matricule);
			 if(matExiste.isPresent())
				{
						throw new Exception("matricule");
					
				}
		 }
			 
			return camionRepository.save(c);
	 }
		 return null;
	
	}


	@Override
	public void suppCamion(long id) {		
		camionRepository.deleteById(id);
	}



}
