package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Camion;
import com.example.demo.service.CamionService;

@CrossOrigin("*")
@RestController
public class CamionController {
	
	@Autowired
	//@Qualifier("CamionServiceImp")
	CamionService camionService ;
	
	@RequestMapping(value = "/ajouterCamion", method = RequestMethod.POST)
	public @ResponseBody Camion ajouterCamion(@RequestBody Camion c ) throws Exception {
		return camionService.ajouterCamion(c);
		
	}

	
	@RequestMapping(value = "/listeCamion", method = RequestMethod.GET)
	public @ResponseBody Iterable<Camion> listeCamion() {
		
		return camionService.listerCamion();
		
	}
	
	@RequestMapping(value = "/majCamion", method = RequestMethod.PUT)
	public @ResponseBody Camion majCamion(@RequestBody Camion c ) throws Exception {
		
		return camionService.majCamion(c);
		
	}
	
	@RequestMapping(value = "/suppCamion", method = RequestMethod.DELETE)
	public @ResponseBody void suppCamion(@RequestParam long id ) {
		
		camionService.suppCamion(id);
		
	}
	
	
}
