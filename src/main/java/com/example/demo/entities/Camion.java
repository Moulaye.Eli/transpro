package com.example.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Camion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(unique=true)
	private String matricule;
	private String modele;
	private String marque;
	private int chargeMaximale;
	

	
	public Camion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Camion(String matricule, String modele, String marque, int chargeMaximale) {
		super();
		this.matricule = matricule;
		this.modele = modele;
		this.marque = marque;
		this.chargeMaximale = chargeMaximale;
	}
	
	


	public Camion(Long id, String matricule, String modele, String marque, int chargeMaximale) {
		super();
		this.id = id;
		this.matricule = matricule;
		this.modele = modele;
		this.marque = marque;
		this.chargeMaximale = chargeMaximale;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMatricule() {
		return matricule;
	}

	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public int getChargeMaximale() {
		return chargeMaximale;
	}

	public void setChargeMaximale(int chargeMaximale) {
		this.chargeMaximale = chargeMaximale;
	}

	
	
	
	
	
}
