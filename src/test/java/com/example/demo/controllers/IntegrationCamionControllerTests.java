package com.example.demo.controllers;

import static org.assertj.core.api.Assertions.assertThat;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.TransproLapApplication;
import com.example.demo.dao.CamionRepository;
import com.example.demo.entities.Camion;

@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment= WebEnvironment.RANDOM_PORT,
		classes = TransproLapApplication.class
		)
public class IntegrationCamionControllerTests {

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private CamionRepository camionRepository;
	
	@Test
	public void ajoutCamionTest() {
	
		// arrange
		Camion req=new Camion("0712AA01", "T400", "Mercedes", 150);
		// act
		ResponseEntity<Camion> resp= restTemplate.postForEntity("/ajouterCamion", req, Camion.class);
		
		//assert
		assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(resp.getBody().getMarque()).isEqualTo(req.getMarque());
		assertThat(resp.getBody().getMatricule()).isEqualTo(req.getMatricule());
		assertThat(resp.getBody().getChargeMaximale()).isEqualTo(req.getChargeMaximale());
		assertThat(resp.getBody().getModele()).isEqualTo(req.getModele());
	}
	
	@Test
	public void majCamionTest() throws  URISyntaxException {
	
		Optional<Camion> opt1=camionRepository.findById((long) 1);
		Camion c= opt1.get();
		
		c=camionRepository.save(c);
		c.setMarque("Nissan");
		c.setChargeMaximale(200);
		c.setModele("X400");
		
		restTemplate.put(new URI("/majCamion") , c);
		
		Optional<Camion> opt2=camionRepository.findById(c.getId());
		Camion resp=opt2.get();
		
		assertThat(resp.getMarque()).isEqualTo(c.getMarque());
		assertThat(resp.getMatricule()).isEqualTo(c.getMatricule());
		assertThat(resp.getChargeMaximale()).isEqualTo(c.getChargeMaximale());
		assertThat(resp.getModele()).isEqualTo(c.getModele());

		
	}
	
	@Test
	public void ajoutCamionExceptionMatriculeTest() {
		
		Camion req=new Camion("0012AA01", "T400", "Mercedes", 150);
		
		ResponseEntity<Camion> resp= restTemplate.postForEntity("/ajouterCamion", req, Camion.class);
		
		assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);

	}
	
	@Test
	public void listeCamionTest() {
		
		ResponseEntity<Object> resp= restTemplate.getForEntity("/listeCamion", Object.class);
		
		assertThat(resp.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(resp.getBody()).isInstanceOf(Collection.class);

	}
	
	
}
